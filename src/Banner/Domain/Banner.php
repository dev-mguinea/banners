<?php

declare(strict_types=1);

namespace Banners\Banner\Domain;

final class Banner
{
    public function __construct(
        private BannerTitle $title
    ) {
    }

    public function title(): BannerTitle
    {
        return $this->title;
    }
}
